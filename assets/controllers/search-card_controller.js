import { Controller } from '@hotwired/stimulus';

/*
 * This is an example Stimulus controller!
 *
 * Any element with a data-controller="hello" attribute will cause
 * this controller to be executed. The name "hello" comes from the filename:
 * hello_controller.js -> "hello"
 *
 * Delete this file or adapt it for your use!
 */


export default class extends Controller {
    url = document.getElementById('search-form').action;
    nameInput = document.getElementById('input-name');
    typeInput = document.getElementById('input-type');
    idInput = document.getElementById('input-id');
    pageSelect = document.getElementById('select-page');
    connect() {
        
        let params = new URL(document.location.toString()).searchParams;
        if (params.get('q')) {
            let name = params.get('q').match(/(?<=name:\*).+?(?=\*)/gm);
            let type = params.get('q').match(/(?<=types:\*).+?(?=\*)/gm);
            let id = params.get('q').match(/(?<=id:\*).+?(?=\*)/gm);
            if (name)
                this.nameInput.value = name[0];
            if (type)
                this.typeInput.value = type[0];
            if (id)
                this.idInput.value = id[0];
        }
    }

    search ({ params }) {
        let queryString = '';        
        let searchQuery = '';        
        let page = params.page;
        let name = this.nameInput.value.trim();
        let type = this.typeInput.value.trim();
        let id = this.idInput.value.trim();
        console.log(params);

        if (page) {
            queryString += this.getQueryStringSeparator(queryString) +'page=' + page;
        }

        if (name) {
            searchQuery += this.getSearchQuerySeparator(searchQuery) + 'name:*' + name + '*';
        }

        if (type) {
            searchQuery += this.getSearchQuerySeparator(searchQuery) + 'types:*' + type + '*';
        }

        if (id) {
            searchQuery += this.getSearchQuerySeparator(searchQuery) + 'id:*' + id + '*';
        }

        queryString += this.getQueryStringSeparator(queryString) + searchQuery
        window.location = this.url +queryString;
    }

    change(){
       this.search({params: {page: this.pageSelect.value}});
    }

    getQueryStringSeparator(queryString) {
        if (!queryString) {
            return "?";
        }

        return '&';
    }

    getSearchQuerySeparator(searchQuery) {
        if (searchQuery) {
            return " ";
        }
        return "q=";
    }
}

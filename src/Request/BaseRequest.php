<?php

namespace App\Request;

class BaseRequest
{
    public function validateQueryStringField(string $field, ?string $queryString): string
    {
        if (!property_exists($this, $field)) 
            throw(new \Exception($this->getPropretyDoesNotExistErrorMessage()));
        if (!str_contains($queryString, $field.'=')) {
            $queryString .= (empty($queryString) ? '' : '&').$field.'='.$this->{$field};
        }

        return $queryString;
    }

    public function getPropretyDoesNotExistErrorMessage(): string
    {
        return 'Class does not have that property';
    }
}
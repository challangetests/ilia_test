<?php

namespace App\Request\CardApi;

use App\Request\BaseRequest;
use Symfony\Component\Validator\Constraints\Type;

class IndexRequest extends BaseRequest
{
    public function __construct(
        #[Type('integer')]
        public ?int $pageSize,

        #[Type('integer')]
        public ?int $page,

        #[Type('string')]
        public ?string $orderBy,

        #[Type('string')]
        public ?string $select,

        #[Type('string')]
        public ?string $q
    ) {
    }

    public function getDefaultQueryString(): string
    {
        return 'pageSize='.$this->pageSize.'&select='.$this->select;
    }

    public function validateQueryString(?string $queryString = null): string
    {
        if (is_null($queryString)) {
            return $this->getDefaultQueryString();
        }

        $queryString = $this->validateQueryStringField('pageSize', $queryString);
        $queryString = $this->validateQueryStringField('select', $queryString);

        return $queryString;
    }
}

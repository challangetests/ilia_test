<?php

namespace App\Request\CardApi;

use App\Request\BaseRequest;
use Symfony\Component\Validator\Constraints\Type;

class ShowRequest extends BaseRequest
{
    public function __construct(
        #[Type('string')]
        public ?string $select
    ) {
    }

    public function getDefaultQueryString(): string
    {
        return 'select='.$this->select;
    }

    public function validateQueryString(?string $queryString): string
    {
        if (is_null($queryString)) {
            return $this->getDefaultQueryString();
        }
        $queryString = $this->validateQueryStringField('select', $queryString);

        return $queryString;
    }
}

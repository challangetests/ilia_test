<?php

namespace App\Client;

use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CardClient
{
    public function __construct(
        private HttpClientInterface $httpClient,
        private string $apiCardUrl,
        private string $apiCardKey
    ) {
    }

    public function index(string $queryString = ''): array
    {
        $response = $this->httpClient->request(
            'GET',
            $this->apiCardUrl.'?'.$queryString,
            [
                'headers' => [
                    'X-Api-Key' => $this->apiCardKey,
                ],
            ]
        );

        if (Response::HTTP_BAD_REQUEST == $response->getStatusCode()) {
            $error = $response->toArray(false);
            throw new BadRequestException($error['error']['message']);
        }

        return $response->toArray();
    }

    public function show(string $id, string $queryString = ''): array
    {
        $response = $this->httpClient->request(
            'GET',
            $this->apiCardUrl.'/'.$id.'?'.$queryString,
            [
                'headers' => [
                    'X-Api-Key' => $this->apiCardKey,
                ],
            ]
        );

        if (Response::HTTP_NOT_FOUND == $response->getStatusCode()) {
            throw new NotFoundHttpException('Card not found');
        }
        $response = $response->toArray();

        if (!isset($response['data']['resistances'])) {
            $response['data']['resistances'] = null;
        }

        return $response;
    }
}

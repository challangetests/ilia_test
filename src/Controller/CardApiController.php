<?php

namespace App\Controller;

use App\Client\CardClient;
use App\Request\CardApi\IndexRequest;
use App\Request\CardApi\ShowRequest;
use OpenApi\Attributes\PathParameter;
use OpenApi\Attributes\QueryParameter;
use OpenApi\Attributes\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Tag(name: 'Cards')]
#[Route('/api/cards')]
class CardApiController extends AbstractController
{
    /**
     * Card Index.
     *
     * Search for one or many cards given a search query.
     */
    #[Route('/', name: 'api_card_index', methods: 'GET')]
    #[QueryParameter(
        name: 'pageSize',
        description: 'The maximum amount of cards to return.',
        example: '10'
    )]
    #[QueryParameter(
        name: 'page',
        description: 'The page of data to access.',
        example: '1'
    )]
    #[QueryParameter(
        name: 'q',
        description: 'The search query.',
        example: 'name:charizard'
    )]
    #[QueryParameter(
        name: 'orderBy',
        description: 'The field(s) to order the results by.',
        example: 'name'
    )]
    #[QueryParameter(
        name: 'select',
        description: 'A comma delimited list of fields to return in the response. 
            By default, all fields are returned if this query parameter is not used.',
        example: 'id,name'
    )]
    public function index(CardClient $cardClient, Request $request, IndexRequest $indexRequest): Response
    {
        $queryString = $indexRequest->validateQueryString($request->getQueryString());

        return $this->json($cardClient->index($queryString));
    }

    /**
     * Card Show.
     *
     * Fetch the details of a single card.
     */
    #[Route('/{id}', name: 'api_card_show', methods: 'GET')]
    #[PathParameter(name: 'id', example: 'sm9-1')]
    #[QueryParameter(
        name: 'select',
        description: 'A comma delimited list of fields to return in the response. 
            By default, all fields are returned if this query parameter is not used.',
        example: 'id,name,attacks'
    )]
    public function show(CardClient $cardClient, Request $request, ShowRequest $showRequest, string $id): Response
    {
        $queryString = $showRequest->validateQueryString($request->getQueryString());

        return $this->json($cardClient->show($id, $queryString));
    }
}

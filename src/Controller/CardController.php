<?php

namespace App\Controller;

use App\Client\CardClient;
use App\Request\CardApi\ShowRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CardController extends AbstractController
{
    #[Route('/card/{id}', name: 'app_card_show')]
    public function home(CardClient $cardClient, string $id, ShowRequest $showRequest): Response
    {
        $card = $cardClient->show($id, $showRequest->getDefaultQueryString());

        return $this->render('card/show.html.twig', [
            'card' => $card['data'],
        ]);
    }
}

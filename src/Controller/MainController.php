<?php

namespace App\Controller;

use App\Client\CardClient;
use App\Request\CardApi\IndexRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    #[Route('/', name: 'app_main_home')]
    public function home(CardClient $cardClient, IndexRequest $indexRequest, Request $request): Response
    {
        $cards = $cardClient->index($indexRequest->validateQueryString($request->getQueryString()));

        return $this->render('main/homepage.html.twig', [
            'cards' => $cards,
        ]);
    }
}

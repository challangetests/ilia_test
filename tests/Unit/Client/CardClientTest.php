<?php

namespace App\Tests\Unit\Client;

use App\Client\CardClient;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * @covers \CardClient
 */
class CardClientTest extends TestCase
{
    public function orderByQueryProvider()
    {
        yield ['id', 'b'];
        yield ['-id', 'x'];
    }

    public function cardIdsProvider()
    {
        yield ['swsh10-4'];
        yield ['swsh35-54'];
        yield ['pop4-10'];
    }

    /**
     * @covers \CardClient::index
     */
    public function testIndexWithoutQueryString(): void
    {
        $mockHttpClient = $this->createMock(HttpClientInterface::class);
        $mockResponse = $this->createMock(ResponseInterface::class);

        $mockResponse
            ->method('toArray')
            ->willReturn([
                'data' => [],
                'page' => 1,
                'pageSize' => 250,
            ]);

        $mockHttpClient
            ->method('request')
            ->willReturn($mockResponse);

        $cardClient = new CardClient($mockHttpClient, $_ENV['API_CARD_URL'], $_ENV['API_CARD_KEY']);
        $response = $cardClient->index();
        $this->assertIsArray($response);
        $this->assertEquals(1, $response['page']);
        $this->assertEquals(250, $response['pageSize']);
    }

    /**
     * @covers \CardClient::index
     */
    public function testIndexPagination(): void
    {
        $mockHttpClient = $this->createMock(HttpClientInterface::class);
        $mockResponse = $this->createMock(ResponseInterface::class);

        $mockResponse
            ->method('toArray')
            ->willReturn([
                'data' => [],
                'page' => 5,
                'pageSize' => 10,
            ]);

        $mockHttpClient
            ->method('request')
            ->willReturn($mockResponse);

        $cardClient = new CardClient($mockHttpClient, $_ENV['API_CARD_URL'], $_ENV['API_CARD_KEY']);
        $response = $cardClient->index('page=5&pageSize=10');

        $this->assertIsArray($response);
        $this->assertEquals(5, $response['page']);
        $this->assertEquals(10, $response['pageSize']);
    }

    /**
     * @covers \CardClient::index
     */
    public function testIndexSelectFields(): void
    {
        $mockHttpClient = $this->createMock(HttpClientInterface::class);
        $mockResponse = $this->createMock(ResponseInterface::class);

        $mockResponse
            ->method('toArray')
            ->willReturn([
                'data' => [
                    [
                        'id' => 'id',
                        'name' => 'Pikachu',
                        'types' => ['type'],
                        'images' => [
                            'small' => "https:\/\/images.io\/img.png",
                            'large' => "https:\/\/images.io\/img_hires.png",
                        ],
                    ],
                ],
            ]);

        $mockHttpClient
            ->method('request')
            ->willReturn($mockResponse);

        $desiredFields = ['id', 'name', 'images', 'types'];
        $cardClient = new CardClient($mockHttpClient, $_ENV['API_CARD_URL'], $_ENV['API_CARD_KEY']);
        $response = $cardClient->index('pageSize=10&select='.implode(',', $desiredFields));

        $this->assertIsArray($response);
        if (is_array($response)) {
            $this->assertArrayHasKey('data', $response);
            $this->assertGreaterThanOrEqual(1, count($response['data']));
            foreach ($desiredFields as $desiredField) {
                $this->assertArrayHasKey($desiredField, $response['data'][0]);
            }
        }
    }

    /**
     * @covers \CardClient::index
     */
    public function testIndexByLikeName(): void
    {
        $mockHttpClient = $this->createMock(HttpClientInterface::class);
        $mockResponse = $this->createMock(ResponseInterface::class);

        $mockResponse
            ->method('toArray')
            ->willReturn([
                'data' => [
                    [
                        'name' => 'Pikachu',
                    ],
                ],
            ]);

        $mockHttpClient
            ->method('request')
            ->willReturn($mockResponse);

        $desiredName = 'ikach';
        $cardClient = new CardClient($mockHttpClient, $_ENV['API_CARD_URL'], $_ENV['API_CARD_KEY']);
        $response = $cardClient->index('pageSize=10&select=name&q=name:*'.$desiredName.'*');

        $this->assertIsArray($response);
        if (is_array($response)) {
            $this->assertArrayHasKey('data', $response);
            $this->assertGreaterThanOrEqual(1, count($response['data']));
            $this->assertStringContainsString($desiredName, $response['data'][0]['name']);
        }
    }

    /**
     * @covers \CardClient::index
     *
     * @dataProvider orderByQueryProvider
     */
    public function testIndexOrderById(string $fieldName, string $fieldFirstLetter): void
    {
        $mockHttpClient = $this->createMock(HttpClientInterface::class);
        $mockResponse = $this->createMock(ResponseInterface::class);

        $mockResponse
            ->method('toArray')
            ->willReturn([
                'data' => [
                    [
                        'id' => $fieldFirstLetter.rand(1, 10000),
                    ],
                ],
            ]);

        $mockHttpClient
            ->method('request')
            ->willReturn($mockResponse);

        $cardClient = new CardClient($mockHttpClient, $_ENV['API_CARD_URL'], $_ENV['API_CARD_KEY']);
        $response = $cardClient->index('pageSize=10&select=id&orderBy='.$fieldName);

        $this->assertIsArray($response);
        if (is_array($response)) {
            $this->assertArrayHasKey('data', $response);
            $this->assertGreaterThanOrEqual(1, count($response['data']));
            $this->assertStringStartsWith($fieldFirstLetter, $response['data'][0]['id']);
        }
    }

    /**
     * @covers \CardClient::index
     */
    public function testIndexBadRequestException(): void
    {
        $mockHttpClient = $this->createMock(HttpClientInterface::class);
        $mockResponse = $this->createMock(ResponseInterface::class);

        $mockResponse
            ->method('getStatusCode')
            ->willReturn(Response::HTTP_BAD_REQUEST);

        $mockResponse
            ->method('toArray')
            ->willReturn([
                'error' => ['message' => 'a'],
            ]);

        $mockHttpClient
            ->method('request')
            ->willReturn($mockResponse);
        try {
            $cardClient = new CardClient($mockHttpClient, $_ENV['API_CARD_URL'], $_ENV['API_CARD_KEY']);
            $cardClient->index('q=asdasdas');
        } catch (\Exception $e) {
            $this->assertInstanceOf(BadRequestException::class, $e);
        }
    }

    /**
     * @covers \CardClient::show
     *
     * @dataProvider cardIdsProvider
     */
    public function testShowWithoutQueryString(string $id): void
    {
        $mockHttpClient = $this->createMock(HttpClientInterface::class);
        $mockResponse = $this->createMock(ResponseInterface::class);

        $mockResponse
            ->method('toArray')
            ->willReturn([
                'data' => [
                    'id' => $id,
                ],
            ]);

        $mockHttpClient
            ->method('request')
            ->willReturn($mockResponse);

        $cardClient = new CardClient($mockHttpClient, $_ENV['API_CARD_URL'], $_ENV['API_CARD_KEY']);
        $response = $cardClient->show($id);

        $this->assertIsArray($response);
        if (is_array($response)) {
            $this->assertArrayHasKey('data', $response);
            $this->assertEquals($id, $response['data']['id']);
        }
    }

    /**
     * @covers \CardClient::show
     *
     * @dataProvider cardIdsProvider
     */
    public function testShowSelectFields(string $id): void
    {
        $mockHttpClient = $this->createMock(HttpClientInterface::class);
        $mockResponse = $this->createMock(ResponseInterface::class);

        $mockResponse
            ->method('toArray')
            ->willReturn([
                'data' => [
                    'id' => $id,
                    'name' => $id,
                    'images' => $id,
                ],
            ]);

        $mockHttpClient
            ->method('request')
            ->willReturn($mockResponse);

        $desiredFields = ['id', 'name', 'images'];
        $cardClient = new CardClient($mockHttpClient, $_ENV['API_CARD_URL'], $_ENV['API_CARD_KEY']);
        $response = $cardClient->show($id);
        $this->assertIsArray($response);
        if (is_array($response)) {
            $this->assertArrayHasKey('data', $response);
            foreach ($desiredFields as $desiredField) {
                $this->assertArrayHasKey($desiredField, $response['data']);
            }
        }
    }

    /**
     * @covers \CardClient::show
     */
    public function testShowBadRequestException(): void
    {
        $mockHttpClient = $this->createMock(HttpClientInterface::class);
        $mockResponse = $this->createMock(ResponseInterface::class);

        $mockResponse
            ->method('getStatusCode')
            ->willReturn(Response::HTTP_NOT_FOUND);

        $mockHttpClient
            ->method('request')
            ->willReturn($mockResponse);
        try {
            $cardClient = new CardClient($mockHttpClient, $_ENV['API_CARD_URL'], $_ENV['API_CARD_KEY']);
            $cardClient->show('asdasdas');
        } catch (\Exception $e) {
            $this->assertInstanceOf(NotFoundHttpException::class, $e);
        }
    }
}

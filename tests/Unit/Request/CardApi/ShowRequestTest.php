<?php

namespace App\Tests\Unit\Request\CardApi;

use App\Request\CardApi\ShowRequest;
use PHPUnit\Framework\TestCase;

/**
 * @covers \ShowRequest
 */
class ShowRequestTest extends TestCase
{
    /**
     * @covers \CardClient::getDefaultQueryString
     */
    public function testGetDefaultQueryStringRandomValue(): void
    {
        $select = rand();
        $showRequest = new ShowRequest($select);
        $desiredString = 'select='.$select;

        $this->assertEquals($desiredString, $showRequest->getDefaultQueryString());
    }

    /**
     * @covers \CardClient::validateQueryStringField
     */
    public function testValidateSelectWithoutParams(): void
    {
        $select = rand();
        $showRequest = new ShowRequest($select);
        $desiredString = 'select='.$select;

        $this->assertEquals($desiredString, $showRequest->validateQueryStringField('select', ''));
    }

    /**
     * @covers \CardClient::validateQueryStringField
     */
    public function testValidateSelectWithRandomValue(): void
    {
        $select = rand();
        $randomString = rand();
        $showRequest = new ShowRequest($select);
        $desiredString = $randomString.'&select='.$select;

        $this->assertEquals($desiredString, $showRequest->validateQueryStringField('select', $randomString));
    }

    /**
     * @covers \CardClient::validateQueryString
     */
    public function testValidateQueryStringWithoutParams(): void
    {
        $select = rand();
        $showRequest = new ShowRequest($select);
        $desiredString = $showRequest->getDefaultQueryString();

        $this->assertEquals($desiredString, $showRequest->validateQueryString(''));
    }

    /**
     * @covers \CardClient::validateQueryString
     */
    public function testValidateQueryStringWithRandomValue(): void
    {
        $select = rand();
        $showRequest = new ShowRequest($select);
        $desiredString = 'select='.$select;

        $this->assertEquals($desiredString, $showRequest->validateQueryString('select='.$select));
    }

    /**
     * @covers \CardClient::validateQueryStringField
     */
    public function testValidateQueryStringWithInvalidProperty(): void
    {
        $select = rand();
        $showRequest = new ShowRequest($select);
        $desiredErrorMsg = $showRequest->getPropretyDoesNotExistErrorMessage();
        try {
            $showRequest->validateQueryStringField($select, $select);
        } catch (\Exception $e) {
            $this->assertEquals($desiredErrorMsg, $e->getMessage());
        }

    }
}

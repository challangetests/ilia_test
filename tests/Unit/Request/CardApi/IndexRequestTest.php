<?php

namespace App\Tests\Unit\Request\CardApi;

use App\Request\CardApi\IndexRequest;
use PHPUnit\Framework\TestCase;

/**
 * @covers \IndexRequest
 */
class IndexRequestTest extends TestCase
{
    /**
     * @covers \CardClient::getDefaultQueryString
     */
    public function testGetDefaultQueryStringRandomValues(): void
    {
        $pageSize = rand();
        $select = rand();
        $indexRequest = new IndexRequest($pageSize, rand(), rand(), $select, rand());
        $desiredString = 'pageSize='.$pageSize.'&select='.$select;

        $this->assertEquals($desiredString, $indexRequest->getDefaultQueryString());
    }

    /**
     * @covers \CardClient::validateQueryStringField
     */
    public function testValidatePageSizeWithoutParams(): void
    {
        $pageSize = rand();
        $indexRequest = new IndexRequest($pageSize, rand(), rand(), rand(), rand());
        $desiredString = 'pageSize='.$pageSize;

        $this->assertEquals($desiredString, $indexRequest->validateQueryStringField('pageSize', ''));
    }

    /**
     * @covers \CardClient::validateQueryStringField
     */
    public function testValidatePageSizeWithRandomParam(): void
    {
        $pageSize = rand();
        $randomString = rand();
        $indexRequest = new IndexRequest($pageSize, rand(), rand(), rand(), rand());
        $desiredString = $randomString.'&pageSize='.$pageSize;

        $this->assertEquals($desiredString, $indexRequest->validateQueryStringField('pageSize', $randomString));
    }

    /**
     * @covers \CardClient::validateQueryStringField
     */
    public function testValidateSelectWithoutParams(): void
    {
        $select = rand();
        $indexRequest = new IndexRequest(rand(), rand(), rand(), $select, rand());
        $desiredString = 'select='.$select;

        $this->assertEquals($desiredString, $indexRequest->validateQueryStringField('select', ''));
    }

    /**
     * @covers \CardClient::validateQueryStringField
     */
    public function testValidateSelectWithRandomParam(): void
    {
        $select = rand();
        $randomString = rand();
        $indexRequest = new IndexRequest(rand(), rand(), rand(), $select, rand());
        $desiredString = $randomString.'&select='.$select;

        $this->assertEquals($desiredString, $indexRequest->validateQueryStringField('select', $randomString));
    }

    /**
     * @covers \CardClient::validateQueryString
     */
    public function testValidateQueryStringWithoutParams(): void
    {
        $pageSize = rand();
        $select = rand();
        $indexRequest = new IndexRequest($pageSize, rand(), rand(), $select, rand());
        $desiredString = $indexRequest->getDefaultQueryString();

        $this->assertEquals($desiredString, $indexRequest->validateQueryString(''));
    }

    /**
     * @covers \CardClient::validateQueryString
     */
    public function testValidateQueryStringWithOnlyPageSize(): void
    {
        $pageSize = rand();
        $select = rand();
        $indexRequest = new IndexRequest(rand(), rand(), rand(), $select, rand());
        $desiredString = 'pageSize='.$pageSize.'&select='.$select;

        $this->assertEquals($desiredString, $indexRequest->validateQueryString('pageSize='.$pageSize));
    }

    /**
     * @covers \CardClient::validateQueryString
     */
    public function testValidateQueryStringWithOnlySelect(): void
    {
        $pageSize = rand();
        $select = rand();
        $indexRequest = new IndexRequest($pageSize, rand(), rand(), rand(), rand());
        $desiredString = 'select='.$select.'&pageSize='.$pageSize;

        $this->assertEquals($desiredString, $indexRequest->validateQueryString('select='.$select));
    }
}

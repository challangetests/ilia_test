## Installing the project

To install the project run the following comands.

- ``` git clone https://gitlab.com/challangetests/ilia_test.git``` To clone the project.
- ``` cd ilia_test``` To acess the project's directory.
- ``` docker-compose up -d``` To create and initialize the project's enviroment.
- add ``` 127.0.0.1 ilia_test.desenv``` to your system hosts ``` /etc/hosts``` for linux systems.
- ``` docker exec -it ilia-php83-fpm bash``` To enter the php-fpm container.
- ``` cd ilia_test``` To acess the project's directory inside container.
- ``` cp .env.example .env``` To create the project's enviroment file.
- Fill the .env file variables with proper values (just API_CARD_KEY with your https://pokemontcg.io/ account api key)
- ``` composer install``` To install the project's dependencies.
- ``` php bin/console tailwind:build``` To build the front end css.

## Project features

To access the projects features, documentation and tests you can do the following:

- Access ``` http://ilia_test.desenv/``` To use the web application.
- Access ``` http://ilia_test.desenv/api/doc``` To see the api documentation.
- Access ``` ./vendor/bin/phpunit --testdox``` To run the unit tests.
